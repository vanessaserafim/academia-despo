package br.gov.serpro.pilhaencapsulada;

public class Principal {

	public static void main(String[] args) {
		Pilha p = new Pilha(10);
		p.empilhar("teste1");
		p.empilhar("teste2");
		p.empilhar("teste3");
		p.empilhar("teste4");
		System.out.println(p.topo());
		System.out.println(p.tamanho());
		
		
		System.out.println("-------------------");
		System.out.println(p.desempilhar());
		System.out.println(p.topo());
		System.out.println(p.tamanho());
		
	}

}
