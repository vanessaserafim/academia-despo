package br.gov.serpro.caixa24h;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import br.gov.serpro.exception.SaldoInsuficienteException;
import br.gov.serpro.exception.TransferenciaInvalidaException;

public class BancoMock extends Banco {
    Date data = new Date(System.currentTimeMillis());

    public BancoMock(String nome, Long numero) {
        super(nome, numero);
    }

    @Override
    public void sacar(BigDecimal valor, Conta conta)
            throws SaldoInsuficienteException {
       Lancamento lancamento = new Lancamento("TRANSFERENCIA", valor.negate(), data);
       conta.registrarLancamento(lancamento);
    }

    @Override
    public void depositar(BigDecimal valor, Conta conta) {
        
        Lancamento operacao = new Lancamento("DEPOSITO", valor, data);
        conta.registrarLancamento(operacao);
      
    }

    @Override
    public BigDecimal consultarSaldo(Conta conta) {        
        return conta.getSaldo();
    }

    @Override
    public List<Lancamento> consultaExtrato(Conta conta, LocalDate dataInicial, LocalDate dataFinal) {
        return conta.consultaLancamento(dataInicial, dataFinal);
    }

    @Override
    public void transferirValor(BigDecimal valorATransferir,
            Conta contaDestino, Conta conta)
            throws TransferenciaInvalidaException {
        Lancamento operacaoOrigem = new Lancamento("TRANSFERENCIA", valorATransferir.negate(), data);
        Lancamento operacaoDestino = new Lancamento("RECEBIEMNTO TRANSFERENCIA", valorATransferir, data);
        conta.registrarLancamento(operacaoOrigem);
        contaDestino.registrarLancamento(operacaoDestino);
        
    }

}
