package br.gov.serpro.cadeiaconstrutor;

public class DoMeio extends PaiDeTodos {

	public DoMeio(String txt) {
		//Ele chama implicitamente o construtor da superclasse
		//Como se tivesse escrito a chamada  super();
		//E so funciona se for um construtor sem parametros
		System.out.println("Construtor DoMeio" + txt);
	}

}
