package br.gov.serpro.bancoBeta;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;
import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.bancoBeta.BancoBeta;
import br.gov.serpro.bancoBeta.ContaPremium;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class BancoBetaTest {

    private List<ContaBancaria> contas = new ArrayList<ContaBancaria>();

    private ContaBancaria contaComum = new ContaComum(12345, BigDecimal.ZERO,
            new BigDecimal("200"));
    private ContaBancaria contaPremium = new ContaPremium(0147, BigDecimal.TEN,
            new BigDecimal("5000"));
    private BancoBeta bancoBeta;
    private Integer numeroConta;

    @Test
    public void adicionarContasBancoBeta()
            throws ContaBancariaInexistenteException {
        contas.add(contaComum);
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
    }

    @Test(expected = IllegalArgumentException.class)
    public void naoPermitirIncluirDuasContasIguaisNoMesmoBanco()
            throws ContaBancariaInexistenteException {
        contas.add(contaComum);
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);

    }

    @Test(expected = RuntimeException.class)
    public void naoTransferirParaContaBancariaInexistente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        ContaBancaria contaInexistente = new ContaComum(90000, BigDecimal.ZERO,
                BigDecimal.ZERO);

        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        bancoBeta.sacarValor(contaInexistente.getNumero(), BigDecimal.TEN);

    }

    @Test(expected = SaldoInsuficienteException.class)
    public void naoPermitirSacarContaComumPorSaldoInsuficiente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.sacarValor(numeroConta, new BigDecimal("300"));
    }

    @Test(expected = SaldoInsuficienteException.class)
    public void naoPermitirSacarContaPremiumPorSaldoInsuficiente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();
        bancoBeta.sacarValor(numeroConta, new BigDecimal("9870000"));

    }

    @Test
    public void permitirSacarUsandoLimiteContaPremium()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.sacarValor(numeroConta, new BigDecimal("10.00"));
        assertEquals("-1.05", bancoBeta.acessarSaldo(numeroConta).toString());
    }

    @Test
    public void permitirSacarContaComum()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();
        bancoBeta.depositarValor(numeroConta, BigDecimal.TEN);
        bancoBeta.sacarValor(numeroConta, new BigDecimal("1.00"));
        assertEquals("8.00", bancoBeta.acessarSaldo(numeroConta).toString());
    }

    @Test
    public void permitirDepositar() throws ContaBancariaInexistenteException,
            OperacaoInvalidaException {
        contas.clear();
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.depositarValor(numeroConta, BigDecimal.TEN);
        assertEquals("10", bancoBeta.acessarSaldo(numeroConta).toString());
    }

    @Test
    public void permitirTransferencia()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaComum);
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
        Integer contaOrigem = contas.get(1).getNumero();
        Integer contaDestino = contas.get(0).getNumero();

        bancoBeta.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        assertEquals("-1.00", bancoBeta.acessarSaldo(contaOrigem).toString());
        assertEquals("10", bancoBeta.acessarSaldo(contaDestino).toString());

    }

    @Test
    public void permitirMaisDeDuasOperacoesNaContaPremium()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.ONE);
        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.ONE);
        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.TEN);
        assertEquals("22", bancoBeta.acessarSaldo(numeroConta).toString());

    }

    @Test(expected = OperacaoInvalidaException.class)
    public void naoPermitirOperacoesPorExcessoDeTransacoes()
            throws ContaBancariaInexistenteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.ONE);
        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.TEN);

    }

    @Test
    public void emitirExtrato() throws OperacaoInvalidaException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaPremium);
        bancoBeta = new BancoBeta(contas);
        numeroConta = contas.get(0).getNumero();

        bancoBeta.acessarSaldo(numeroConta);
        bancoBeta.depositarValor(numeroConta, BigDecimal.ONE);
        assertNotNull(bancoBeta.acessarExtrato(numeroConta));
    }

    @Test
    public void emitirExtratoEmPeriodo() throws OperacaoInvalidaException,
            SaldoInsuficienteException, ContaBancariaInexistenteException {
        contas.clear();
        contas.add(contaPremium);
        contas.add(contaComum);
        bancoBeta = new BancoBeta(contas);
        Integer contaOrigem = contas.get(0).getNumero();
        Integer contaDestino = contas.get(1).getNumero();

        bancoBeta.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        bancoBeta.transferirValor(contaOrigem, contaDestino, BigDecimal.ONE);
        bancoBeta.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        bancoBeta.sacarValor(contaDestino, BigDecimal.TEN);
        assertNotNull(bancoBeta.acessarExtrato(contaOrigem,
                LocalDate.of(2018, 12, 01), LocalDate.now()));
        assertNotNull(bancoBeta.acessarExtrato(contaDestino,
                LocalDate.of(2018, 12, 01), LocalDate.now()));
    }

}
