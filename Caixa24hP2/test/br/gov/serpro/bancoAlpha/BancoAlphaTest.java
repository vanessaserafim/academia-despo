package br.gov.serpro.bancoAlpha;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.*;
import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.bancoAlpha.BancoAlpha;
import br.gov.serpro.bancoAlpha.ContaComum;
import br.gov.serpro.bancoAlpha.ContaEspecial;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class BancoAlphaTest {

    private List<ContaBancaria> contas = new ArrayList<ContaBancaria>();

    private ContaBancaria contaComum = new ContaComum(12345, BigDecimal.ZERO,
            BigDecimal.ZERO);
    private ContaBancaria contaEspecial = new ContaEspecial(56789,
            BigDecimal.TEN, new BigDecimal("1000"));
    private BancoAlpha bancoAlpha;
    ArrayList<ContaBancaria> retorno = new ArrayList<ContaBancaria>();

    @Test
    public void adicionarContasBancoAlpha()
            throws ContaBancariaInexistenteException {
        contas.add(contaComum);
        contas.add(contaEspecial);
        bancoAlpha = new BancoAlpha(contas);

    }

    @Test(expected = IllegalArgumentException.class)
    public void naoPermitirIncluirDuasContasIguaisNoMesmoBanco()
            throws ContaBancariaInexistenteException {
        contas.add(contaComum);
        contas.add(contaComum);
        bancoAlpha = new BancoAlpha(contas);

    }

    @Test(expected = RuntimeException.class)
    public void naoTransferirParaContaBancariaInexistente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        ContaBancaria contaInexistente = new ContaComum(90000, BigDecimal.ZERO,
                BigDecimal.ZERO);
        contas.add(contaComum);
        bancoAlpha.sacarValor(contaInexistente.getNumero(), BigDecimal.TEN);

    }

    @Test(expected = SaldoInsuficienteException.class)
    public void naoPermitirSacarContaComumPorSaldoInsuficiente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaComum);
        bancoAlpha = new BancoAlpha(contas);
        bancoAlpha.sacarValor(contas.get(0).getNumero(), new BigDecimal("300"));
    }

    @Test(expected = SaldoInsuficienteException.class)
    public void naoPermitirSacarContaEspecialPorSaldoInsuficiente()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaEspecial);
        bancoAlpha = new BancoAlpha(contas);
        bancoAlpha.sacarValor(contas.get(0).getNumero(), new BigDecimal(
                "9870000"));

    }

    @Test(expected = OperacaoInvalidaException.class)
    public void naoPermitirDepositarBancoAlpha()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaComum);
        bancoAlpha = new BancoAlpha(contas);
        bancoAlpha.depositarValor(contas.get(0).getNumero(), BigDecimal.TEN);
    }

    @Test
    public void permitirSacarUsandoLimiteContaEspecial()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaEspecial);
        bancoAlpha = new BancoAlpha(contas);
        Integer numeroConta = contas.get(0).getNumero();
        bancoAlpha.sacarValor(numeroConta, new BigDecimal("500"));
        assertEquals("-490", bancoAlpha.acessarSaldo(numeroConta).toString());
    }

    @Test
    public void permitirTransferencia()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        contas.clear();
        contas.add(contaComum);
        contas.add(contaEspecial);
        bancoAlpha = new BancoAlpha(contas);
        Integer contaOrigem = contas.get(1).getNumero();
        Integer contaDestino = contas.get(0).getNumero();

        bancoAlpha.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        assertEquals("0", bancoAlpha.acessarSaldo(contaOrigem).toString());
        assertEquals("10", bancoAlpha.acessarSaldo(contaDestino).toString());

    }

    @Test
    public void permitirMaisDeTresOperacoesNaContaEspecial()
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException, OperacaoInvalidaException {
        contas.clear();
        contas.add(contaEspecial);
        bancoAlpha = new BancoAlpha(contas);
        bancoAlpha.acessarSaldo(contas.get(0).getNumero());
        bancoAlpha.sacarValor(contas.get(0).getNumero(), new BigDecimal("1"));
        bancoAlpha.sacarValor(contas.get(0).getNumero(), new BigDecimal("2"));
        bancoAlpha.sacarValor(contas.get(0).getNumero(), new BigDecimal("3"));
        assertEquals("4", bancoAlpha.acessarSaldo(contas.get(0).getNumero())
                .toString());

    }

    @Test
    public void emitirExtratoGeral() throws OperacaoInvalidaException,
            SaldoInsuficienteException, ContaBancariaInexistenteException {
        contas.clear();
        contas.add(contaEspecial);
        contas.add(contaComum);
        bancoAlpha = new BancoAlpha(contas);
        Integer contaOrigem = contas.get(0).getNumero();
        Integer contaDestino = contas.get(1).getNumero();

        bancoAlpha.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        assertNotNull(bancoAlpha.acessarExtrato(contaOrigem));
        assertNotNull(bancoAlpha.acessarExtrato(contaDestino));
    }

    @Test
    public void emitirExtratoEmPeriodo() throws OperacaoInvalidaException,
            SaldoInsuficienteException, ContaBancariaInexistenteException {
        contas.clear();
        contas.add(contaEspecial);
        contas.add(contaComum);
        bancoAlpha = new BancoAlpha(contas);
        Integer contaOrigem = contas.get(0).getNumero();
        Integer contaDestino = contas.get(1).getNumero();

        bancoAlpha.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        bancoAlpha.transferirValor(contaOrigem, contaDestino, BigDecimal.ONE);
        bancoAlpha.transferirValor(contaOrigem, contaDestino, BigDecimal.TEN);
        bancoAlpha.sacarValor(contaDestino, BigDecimal.TEN);
        assertNotNull(bancoAlpha.acessarExtrato(contaOrigem,
                LocalDate.of(2018, 12, 01), LocalDate.now()));
        assertNotNull(bancoAlpha.acessarExtrato(contaDestino,
                LocalDate.of(2018, 12, 01), LocalDate.now()));
    }

}
