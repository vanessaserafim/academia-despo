package br.gov.serpro.caixa24h;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

import br.gov.serpro.banco.Banco;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class TestaCaixa24h {

	private Caixa24h caixaEletronico;
	private Banco banco;
	private Integer numeroConta, numeroOutraConta;
	
	public TestaCaixa24h() throws ContaBancariaInexistenteException {
		//banco = new Bradesco(ContaCorrente); // um exemplo
	}
	
    @Test(expected = ContaBancariaInexistenteException.class)
    public void contaNaoExiste() throws ContaBancariaInexistenteException{
		caixaEletronico = new Caixa24h(banco, numeroOutraConta);
    }
    
 /*       @Test
	public void consultarExtratoDepoisDe1Deposito() throws ContaBancariaInexistenteException {
    	caixaEletronico = new Caixa24h(banco, numeroConta);
		String extratoAnterior = caixaEletronico.consultarExtrato();    	
    	caixaEletronico.realizarDeposito(new BigDecimal("140.50"));
    	extratoAnterior.concat("CREDITO: 140.50 DATA: 05/12/2018");    	
        assertEquals(extratoAnterior, caixaEletronico.consultarExtrato());    	
    }
	
    @Test
	public void consultarExtratoDepoisDe1Saque() throws SaldoInsuficienteException, ContaBancariaInexistenteException {
    	caixaEletronico = new Caixa24h(banco, numeroConta);
		String extratoAnterior = caixaEletronico.consultarExtrato();    	
    	caixaEletronico.realizarSaque(new BigDecimal("10.50"));
    	extratoAnterior.concat("DEBITO: 10.50 DATA: 06/12/2018");    	
        assertEquals(extratoAnterior, caixaEletronico.consultarExtrato());    	
    }
    
    @Test
    public void consultarSaldoDepoisDe1Deposito() throws ContaBancariaInexistenteException{
		caixaEletronico = new Caixa24h(banco, numeroConta);
		BigDecimal saldoAnterior = caixaEletronico.consultarSaldo();
		caixaEletronico.realizarDeposito(new BigDecimal("50"));
			
        assertEquals(saldoAnterior.add(new BigDecimal("50")), caixaEletronico.consultarSaldo());
    }
     
    @Test
    public void consultarSaldoDepoisDe1Saque() throws ContaBancariaInexistenteException, SaldoInsuficienteException{
		caixaEletronico = new Caixa24h(banco, numeroConta);
		BigDecimal saldoAnterior = caixaEletronico.consultarSaldo();
		caixaEletronico.realizarSaque(new BigDecimal("50"));
			
        assertEquals(saldoAnterior.add(new BigDecimal("50")), caixaEletronico.consultarSaldo());
    }
    
    @Test
    public void realizarTransferenciaComSucesso() throws ContaBancariaInexistenteException, SaldoInsuficienteException {
    	caixaEletronico = new Caixa24h(banco, numeroConta);
		BigDecimal saldoAnteriorConta = caixaEletronico.consultarSaldo();
		
		// numeroOutraConta está contida no banco
		Caixa24h outroCaixaEletronico = new Caixa24h(banco, numeroOutraConta);
		BigDecimal saldoAnteriorOutraConta = outroCaixaEletronico.consultarSaldo();
		
		caixaEletronico.realizarTransferencia(numeroOutraConta, new BigDecimal("100"));
		assertEquals(saldoAnteriorConta.subtract(new BigDecimal("100")), caixaEletronico.consultarSaldo());
		assertEquals(saldoAnteriorOutraConta.add(new BigDecimal("100")), outroCaixaEletronico.consultarSaldo());
    }

    @Test(expected = ContaBancariaInexistenteException.class)
    public void contaDestinoNaoExisteNaTransferencia()  throws ContaBancariaInexistenteException, SaldoInsuficienteException {
    	caixaEletronico = new Caixa24h(banco, numeroConta);
		caixaEletronico.realizarTransferencia(numeroOutraConta, new BigDecimal("100"));
    }

    @Test(expected = SaldoInsuficienteException.class)
    public void saldoInsuficienteNaTransferencia() throws ContaBancariaInexistenteException, SaldoInsuficienteException {
    	caixaEletronico = new Caixa24h(banco, numeroConta);

    	// numeroOutraConta está contida no banco
		Caixa24h outroCaixaEletronico = new Caixa24h(banco, numeroOutraConta);
		BigDecimal saldoAnteriorOutraConta = outroCaixaEletronico.consultarSaldo();

    	caixaEletronico.realizarTransferencia(numeroOutraConta, new BigDecimal("100"));
    }
    
    @Test
    public void fazDepositoComSucesso() throws ContaBancariaInexistenteException{
		BigDecimal saldoAnterior = caixaEletronico.consultarSaldo();
		caixaEletronico = new Caixa24h(banco, numeroConta);
		caixaEletronico.realizarDeposito(new BigDecimal("200"));
		assertEquals(saldoAnterior.add(new BigDecimal("200")), caixaEletronico.consultarSaldo());
    }
    
    @Test
    public void fazSaqueComSucesso() throws SaldoInsuficienteException{
   		BigDecimal saldoAnterior = caixaEletronico.consultarSaldo();
		caixaEletronico.realizarSaque(new BigDecimal("100"));
    	assertEquals(saldoAnterior.subtract(new BigDecimal("100")), caixaEletronico.consultarSaldo());			
    }    */
}
