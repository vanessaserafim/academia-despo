package br.gov.serpro.bancoBeta;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.gov.serpro.banco.Banco;
import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.banco.Extrato;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class BancoBeta implements Banco {

    Map<Integer, ContaBancaria> contas = new HashMap<Integer, ContaBancaria>();
    private static final BigDecimal TAXA_TRANSFERENCIA = new BigDecimal("1.00");
    private static final BigDecimal TAXA_FIXA_SAQUE = new BigDecimal("1.00");
    private static final BigDecimal TAXA_PERCENTUAL_SAQUE = new BigDecimal(
            "0.005");
    private static final BigDecimal TAXA_EXTRATO = new BigDecimal("0.50");
    private Date dataHoje = new Date(System.currentTimeMillis());

    public BancoBeta(List<ContaBancaria> contas2) {
        for (ContaBancaria c : contas2) {
            if (contas.containsKey(c.getNumero())) {
                throw new IllegalArgumentException("Conta duplicada");

            }
            contas.put(c.getNumero(), c);
        }
    }

    @Override
    public void validarExistenciaConta(Integer numeroConta)
            throws ContaBancariaInexistenteException {
        if (numeroConta != null) {
            if (!contas.containsKey(numeroConta)) {
                throw new ContaBancariaInexistenteException(
                        "Conta Bancaria Inexistente", numeroConta);
            }
        } else {
            throw new IllegalArgumentException("Numero da conta nulo");
        }

    }

    @Override
    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    public List<Extrato> acessarExtrato(Integer numeroConta)
            throws SaldoInsuficienteException {

        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
            contas.get(numeroConta).getExtratoBancario();
            contas.get(numeroConta).sacar(TAXA_EXTRATO);
            contas.get(numeroConta).adicionarLancamentoBancario(
                    new Extrato(Operacao.DEBITO, TAXA_EXTRATO.negate(),
                            LocalDate.now()));
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�lida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Opera��o inv�lida");
        }
        return contas.get(numeroConta).getExtratoBancario();
    }

    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    public List<Extrato> acessarExtrato(Integer numeroConta,
            LocalDate periodoInicial, LocalDate periodoFinal) {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�llida");
        }
        return contas.get(numeroConta).exibirExtrato(periodoInicial,
                periodoFinal);
    }

    @Override
    public BigDecimal acessarSaldo(Integer numeroConta) {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Opera��o inv�lida");
        }
        return contas.get(numeroConta).getSaldo();
    }

    @Override
    public void transferirValor(Integer numeroContaOrigem,
            Integer numeroContaDestino, BigDecimal valorTransferido)
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        try {
            // Validar conta destino
            validarExistenciaConta(numeroContaOrigem);
            validarExistenciaConta(numeroContaDestino);
            contas.get(numeroContaOrigem).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Opera��o inv�lida");
        }
        // Realizar transferencia
        contas.get(numeroContaOrigem).sacar(valorTransferido);
        contas.get(numeroContaDestino).depositar(valorTransferido);
        // Taxar operacao da conta
        contas.get(numeroContaOrigem).sacar(TAXA_TRANSFERENCIA);
        // Lancar historico no extrato
        contas.get(numeroContaOrigem).adicionarLancamentoBancario(
                new Extrato(Operacao.DEBITO, TAXA_TRANSFERENCIA.negate(),
                        LocalDate.now()));
        contas.get(numeroContaOrigem).adicionarLancamentoBancario(
                new Extrato(Operacao.DEBITO, valorTransferido.negate(),
                        LocalDate.now()));
        contas.get(numeroContaDestino)
                .adicionarLancamentoBancario(
                        new Extrato(Operacao.CREDITO, valorTransferido,
                                LocalDate.now()));
    }

    @Override
    public void depositarValor(Integer numeroConta, BigDecimal valorDepositado)
            throws OperacaoInvalidaException {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        }
        contas.get(numeroConta).depositar(valorDepositado);

    }

    @Override
    public void sacarValor(Integer numeroConta, BigDecimal saque)
            throws SaldoInsuficienteException {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Opera��o inv�lida");
        }
        // Efetiva operacao
        contas.get(numeroConta).sacar(saque);
        // Taxar operacao da conta
        // MathContext mathContext = new MathContext(1,RoundingMode.FLOOR);
        BigDecimal taxa = new BigDecimal(TAXA_FIXA_SAQUE.add(
                saque.multiply(TAXA_PERCENTUAL_SAQUE, MathContext.UNLIMITED)
                        .setScale(2, RoundingMode.FLOOR)).toString());
        contas.get(numeroConta).sacar(taxa);
        // Lancar historico no extrato
        Extrato extratoTaxaConta = new Extrato(Operacao.DEBITO, taxa.negate(),
                LocalDate.now());
        contas.get(numeroConta).adicionarLancamentoBancario(
                new Extrato(Operacao.DEBITO, saque, LocalDate.now()));
        contas.get(numeroConta).adicionarLancamentoBancario(extratoTaxaConta);
    }
}
