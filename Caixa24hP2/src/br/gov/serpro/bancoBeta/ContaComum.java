package br.gov.serpro.bancoBeta;

import java.math.BigDecimal;
import java.util.Date;

import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.exceptions.OperacaoInvalidaException;

public class ContaComum extends ContaBancaria {

    private int operacoes = 0;
    Date dataHoje = new Date(System.currentTimeMillis());

    public ContaComum(Integer numero, BigDecimal saldo, BigDecimal limite) {
        super(numero, saldo, limite);
    }

    public void verificarOperacoes(Date diaAtual)
            throws OperacaoInvalidaException {
        if (operacoes > 2 || diaAtual == dataHoje) {
            throw new OperacaoInvalidaException(
                    "Limite diario de operacoes excedido");
        } else
            operacoes = operacoes + 1;
    }

}
