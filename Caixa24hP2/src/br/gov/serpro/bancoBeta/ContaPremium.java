package br.gov.serpro.bancoBeta;

import java.math.BigDecimal;
import java.util.Date;

import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.exceptions.OperacaoInvalidaException;

public class ContaPremium extends ContaBancaria {

    public ContaPremium(Integer numero, BigDecimal saldo, BigDecimal limite) {
        super(numero, saldo, limite);
    }

    @Override
    public void verificarOperacoes(Date dataHoje)
            throws OperacaoInvalidaException {

    }

}
