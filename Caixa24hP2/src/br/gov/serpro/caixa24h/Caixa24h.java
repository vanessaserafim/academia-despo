package br.gov.serpro.caixa24h;

import java.math.BigDecimal;
import java.util.List;

import br.gov.serpro.banco.Banco;
import br.gov.serpro.banco.Extrato;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class Caixa24h {

    private Banco bancoConectado;
    private Integer numeroContaConectada;

    private static final String CONTA_INEXISTENTE = "Conta Bancaria Inexistente neste Banco";

    // Retirado c�digo referente a captura de exce��o
    public Caixa24h(Banco bancoConectado, Integer numeroContaConectada)
            throws ContaBancariaInexistenteException {
        this.bancoConectado = bancoConectado;
        this.bancoConectado.validarExistenciaConta(numeroContaConectada);
        this.numeroContaConectada = numeroContaConectada;

    }

    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    public List<Extrato> consultarExtrato() throws SaldoInsuficienteException {
        return bancoConectado.acessarExtrato(numeroContaConectada);
    }

    public BigDecimal consultarSaldo() {
        return bancoConectado.acessarSaldo(numeroContaConectada);
    }

    public void realizarTransferencia(Integer contaDestino,
            BigDecimal valorTransferido)
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        bancoConectado.transferirValor(numeroContaConectada, contaDestino,
                valorTransferido);
    }

    public void realizarDeposito(BigDecimal valorDepositado)
            throws OperacaoInvalidaException {
        bancoConectado.depositarValor(numeroContaConectada, valorDepositado);
    }

    // Alteracao do tipo de retorno para void pois na classe contabancaria o
    // metodo est� void
    public void realizarSaque(BigDecimal saque)
            throws SaldoInsuficienteException {
        bancoConectado.sacarValor(numeroContaConectada, saque);
    }
}