package br.gov.serpro.exceptions;

public class ContaBancariaInexistenteException extends Exception {
    private static final long serialVersionUID = 1L;

    private Integer numero;

	public ContaBancariaInexistenteException(String message, Integer numero) {
		super(message);
		this.numero = numero;
	}

	public Integer getNumero() {
		return numero;
	}
}
