package br.gov.serpro.exceptions;

import java.math.BigDecimal;

public class SaldoInsuficienteException extends Exception {
    private static final long serialVersionUID = 1L;

    private BigDecimal saldo;

	public SaldoInsuficienteException(String message, BigDecimal saldo) {
		super(message);
		this.saldo = saldo;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}
}