package br.gov.serpro.bancoAlpha;

import java.math.BigDecimal;
import java.util.Date;

import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.exceptions.OperacaoInvalidaException;

// BAnco Alpha -  As comuns t�m limite de3 opera��es por dia e n�o podem ficar negativas.

public class ContaComum extends ContaBancaria {

    private int operacoes;
    Date dataHoje = new Date(System.currentTimeMillis());

    public ContaComum(Integer numero, BigDecimal saldo, BigDecimal limite) {
        super(numero, saldo, limite);
    }

    public void verificarOperacoes(Date diaAtual)
            throws OperacaoInvalidaException {
        if (operacoes > 3 || diaAtual == dataHoje) {
            throw new OperacaoInvalidaException(
                    "Limite diario de operacoes excedido");
        } else
            operacoes = operacoes + 1;
    }

}
