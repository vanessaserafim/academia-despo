package br.gov.serpro.bancoAlpha;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.gov.serpro.banco.Banco;
import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.banco.Extrato;
import br.gov.serpro.banco.Operacao;
import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public class BancoAlpha implements Banco {

    Map<Integer, ContaBancaria> contas = new HashMap<Integer, ContaBancaria>();
    private Date dataHoje = new Date(System.currentTimeMillis()); 

    public BancoAlpha(List<ContaBancaria> contas2) {
        for (ContaBancaria conta : contas2) {
            if (contas.containsKey(conta.getNumero())) {
                throw new IllegalArgumentException("Conta duplicada");

            }
            contas.put(conta.getNumero(), conta);
        }
    }

    @Override
    public void validarExistenciaConta(Integer numeroConta)
            throws ContaBancariaInexistenteException {
        if (numeroConta != null) {
            if (!contas.containsKey(numeroConta)) {
                throw new ContaBancariaInexistenteException(
                        "Conta Bancaria Inexistente", numeroConta);
            }
        } else {
            throw new IllegalArgumentException("Numero da conta nulo");
        }
    }

    @Override
    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    public List<Extrato> acessarExtrato(Integer numeroConta) {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�llida");
        }
        return contas.get(numeroConta).getExtratoBancario();
    }

    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    public List<Extrato> acessarExtrato(Integer numeroConta,
            LocalDate periodoInicial, LocalDate periodoFinal) {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�llida");
        }
        return contas.get(numeroConta).exibirExtrato(periodoInicial,
                periodoFinal);
    }

    @Override
    public BigDecimal acessarSaldo(Integer numeroConta) {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�llida");
        }
        return contas.get(numeroConta).getSaldo();
    }

    @Override
    public void transferirValor(Integer numeroContaOrigem,
            Integer numeroContaDestino, BigDecimal valorTransferido)
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException {
        try {
            contas.get(numeroContaOrigem).verificarOperacoes(dataHoje);
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�llida");
        }
        Extrato extratoContaDestino = new Extrato(Operacao.CREDITO,
                valorTransferido.negate(), LocalDate.now());
        Extrato extratoContaOrigem = new Extrato(Operacao.DEBITO,
                valorTransferido, LocalDate.now());
        validarExistenciaConta(numeroContaDestino);
        contas.get(numeroContaDestino).depositar(valorTransferido);
        contas.get(numeroContaOrigem).sacar(valorTransferido);
        contas.get(numeroContaDestino).adicionarLancamentoBancario(
                extratoContaDestino);
        contas.get(numeroContaOrigem).adicionarLancamentoBancario(
                extratoContaOrigem);

    }

    @Override
    public void depositarValor(Integer numeroConta, BigDecimal valorDepositado)
            throws OperacaoInvalidaException {
        throw new OperacaoInvalidaException(
                "N�o � permitido realizar dep�sitos");

    }

    @Override
    public void sacarValor(Integer numeroConta, BigDecimal saque)
            throws SaldoInsuficienteException {
        try {
            validarExistenciaConta(numeroConta);
            contas.get(numeroConta).verificarOperacoes(dataHoje);
        } catch (ContaBancariaInexistenteException e) {
            throw new RuntimeException("Conta Inv�llida");
        } catch (OperacaoInvalidaException e) {
            throw new RuntimeException("Operacao Inv�lida");
        }
        contas.get(numeroConta).sacar(saque);
        contas.get(numeroConta).adicionarLancamentoBancario(
                new Extrato(Operacao.DEBITO, saque, LocalDate.now()));
    }

}
