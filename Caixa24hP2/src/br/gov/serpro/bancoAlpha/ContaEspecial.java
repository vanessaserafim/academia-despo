package br.gov.serpro.bancoAlpha;

import java.math.BigDecimal;
import java.util.Date;

import br.gov.serpro.banco.ContaBancaria;
import br.gov.serpro.exceptions.OperacaoInvalidaException;

public class ContaEspecial extends ContaBancaria {

    Date dataHoje = new Date(System.currentTimeMillis());

    public ContaEspecial(Integer numero, BigDecimal saldo, BigDecimal limite) {
        super(numero, saldo, limite);
    }

    @Override
    public void verificarOperacoes(Date dataHoje)
            throws OperacaoInvalidaException {
    }

}
