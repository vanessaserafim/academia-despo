package br.gov.serpro.banco;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public abstract class ContaBancaria {
    private Integer numero;
    private BigDecimal saldo;
    private BigDecimal limite;
    private List<Extrato> extratoBancario;

    private static final int MAIOR = 1;
    private static final int IGUAL = 0;
    private static final String SALDO_INSUFICIENTE = "Saldo Insuficiente";

    public ContaBancaria(Integer numero, BigDecimal saldo, BigDecimal limite) {
        this.numero = numero;
        this.saldo = saldo;
        this.limite = limite;
        this.extratoBancario = new ArrayList<>();
    }

    // Sugest�o feedback: Alterado nome metodo
    // exibeExtrato retorna uma String que na primeira revis�o foi indicado o
    // uso de um retorno mais estruturado
    public List<Extrato> exibirExtrato(LocalDate periodoInicial,
            LocalDate periodoFinal) {
        List<Extrato> extratoFiltrado = new ArrayList<Extrato>();
        for (Extrato extrato : extratoBancario) {
            if (!(extrato.getDataOperacao().isBefore(periodoInicial) || extrato
                    .getDataOperacao().isAfter(periodoFinal))) {
                extratoFiltrado.add(extrato);
            }
        }
        return extratoFiltrado;

    }

    // Sugest�o feedback: Alterado nome metodo
    public void sacar(BigDecimal valor) throws SaldoInsuficienteException {

        switch (saldo.add(limite).compareTo(valor)) {
        case MAIOR:
        case IGUAL:
            saldo = saldo.subtract(valor);
            break;

        default:
            throw new SaldoInsuficienteException(SALDO_INSUFICIENTE, valor);
        }
    }

    // Sugest�o feedback: Alterado nome metodo
    // Alterado a visibilidade do metodo para poder acessar o metodo
    public void depositar(BigDecimal valor) {
        saldo = saldo.add(valor);
    }

    // Alterado a visibilidade do metodo para poder acessar o metodo
    public Integer getNumero() {
        return numero;
    }

    // Alterado a visibilidade do metodo para poder acessar o metodo
    public BigDecimal getSaldo() {
        return saldo;
    }

    protected BigDecimal getLimite() {
        return limite;
    }

    // Alterado a visibilidade do metodo para poder acessar o metodo
    public List<Extrato> getExtratoBancario() {
        return extratoBancario;
    }

    // Alterado a visibilidade do metodo para poder acessar o metodo
    public void adicionarLancamentoBancario(Extrato extratoBancario) {
        this.extratoBancario.add(extratoBancario);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((numero == null) ? 0 : numero.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContaBancaria other = (ContaBancaria) obj;
        if (numero == null) {
            if (other.numero != null)
                return false;
        } else if (!numero.equals(other.numero))
            return false;
        return true;
    }

    // Adicionado m�todo para verificar o limte de operacoes da conta
    public abstract void verificarOperacoes(Date dataHoje)
            throws OperacaoInvalidaException;
}