package br.gov.serpro.banco;

import java.math.BigDecimal;
import java.time.LocalDate;


public  class Extrato {
	
	private final Operacao operacao;
	private final BigDecimal lancamento;
	private final LocalDate dataOperacao;

	public Extrato(Operacao operacao, BigDecimal lancamento, LocalDate dataOperacao) {
		//super();
		this.operacao = operacao;
		this.lancamento = lancamento;
		this.dataOperacao = dataOperacao;
	}

	protected LocalDate getDataOperacao() {
		return dataOperacao;
	}

	protected BigDecimal getLancamento() {
		return lancamento;
	}

	protected Operacao getOperacao() {
		return operacao;
	}
}