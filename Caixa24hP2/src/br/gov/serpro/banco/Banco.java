package br.gov.serpro.banco;

import java.math.BigDecimal;
import java.util.List;

import br.gov.serpro.exceptions.ContaBancariaInexistenteException;
import br.gov.serpro.exceptions.OperacaoInvalidaException;
import br.gov.serpro.exceptions.SaldoInsuficienteException;

public interface Banco {

    void validarExistenciaConta(Integer numeroConta)
            throws ContaBancariaInexistenteException;

    // - o exibeExtrato retorna uma String que na primeira revis�o foi indicado
    // o uso de um retorno mais estruturado
    // Tambem � necessario incluir a excecao de saldo insufiente pois no banco
    // beta ha cobran�a de taxa
    List<Extrato> acessarExtrato(Integer numeroConta)
            throws SaldoInsuficienteException;

    BigDecimal acessarSaldo(Integer numeroConta);

    void transferirValor(Integer numeroContaOrigem, Integer numeroContaDestino,
            BigDecimal valorTransferido)
            throws ContaBancariaInexistenteException,
            SaldoInsuficienteException;

    // Inclus�o de exce��o para oepracao que um banco pode nao realizar
    void depositarValor(Integer numeroConta, BigDecimal valorDepositado)
            throws OperacaoInvalidaException;

    // Alteracao do tipo de retorno para void pois na classe contabancaria o
    // metodo est� void
    void sacarValor(Integer numeroConta, BigDecimal saque)
            throws SaldoInsuficienteException;

}