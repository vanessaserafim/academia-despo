package br.gov.serpro.pedido;

public class UML {
/**
 * 
 * @startuml
class Cliente [[java:br.gov.serpro.pedido.Cliente]] {
    ~String Nome
    ~String Endereco
}


class Pedido [[java:br.gov.serpro.pedido.Pedido]] {
    ~Date data
    ~boolean status
    +double calcularTaxa()
    +double calcularTotal()
    +double calcularTotalPeso()
}

Cliente "1" --> "0..*"  Pedido : faz 


class DetalhePedido [[java:br.gov.serpro.pedido.DetalhePedido]] {
    ~int quantidade
    ~boolean statusImposto
    +double calcularSubtotal()
    +double calcularPeso()
}

Pedido "1" o-- "1..*"  DetalhePedido : tem 


class Item [[java:br.gov.serpro.pedido.Item]] {
    ~double pesoDaEntrega
    ~double descricao
    +double getPrecoPelaQuantidade()
    +double getPeso()
}

DetalhePedido "0..*" *--  "1"  Item : tem
DetalhePedido --> Item  


class Pagamento [[java:br.gov.serpro.pedido.Pagamento]] {
    ~int quantidade
}

Pedido "1" --> "1..*"  Pagamento : faz


class CartaoDeCredito [[java:br.gov.serpro.pedido.CartaoDeCredito]] {
    ~long numeroCartao
    ~String tipo
    ~Date dataExpedicao
    +boolean isAutorizado()
}

Pagamento <|-- CartaoDeCredito


class Dinheiro [[java:br.gov.serpro.pedido.Dinheiro]] {
    ~double dinheiroOferecido
}

Pagamento <|-- Dinheiro


class Cheque [[java:br.gov.serpro.pedido.Cheque]] {
    ~String nomeBanco
    ~int IDBanco
    +boolean isAutorizado()
}

Pagamento <|-- Cheque



@enduml
     * 
     * 
     * 
     */

}
