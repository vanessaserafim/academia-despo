package br.gov.serpro.pedido;

public abstract  class Pagamento {
    
    private int quantidade;
    
 
	public abstract String getTipoPagamento();


	public int getQuantidade() {
		return quantidade;
	}


	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	

}
