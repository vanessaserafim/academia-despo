package br.gov.serpro.pedido;

public class Principal {

	public static void main(String[] args) {
		
		Pagamento pagto = new CartaoDeCredito();
		Pagar efetivar = new Pagar();
		efetivar.retornarPagamento(pagto);
		
		Pagamento pagto1 = new Cheque();
		efetivar.retornarPagamento(pagto1);
		
		Pagamento pagto2 = new Dinheiro();
		efetivar.retornarPagamento(pagto2);

	}

}
