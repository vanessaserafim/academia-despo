package br.gov.serpro.pedido;

import java.util.Date;

public abstract class Cartao extends Pagamento  {
	
	long numero;
	Date vencimento;
	String nome;
	
	public abstract boolean validar();
	
	public abstract boolean bloquearCartao();

}
