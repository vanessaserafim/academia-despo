package br.gov.serpro.tv;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TesteControleRemoto {
	TV televisao;
	ControleRemoto controle;
	
	@Before
	public void iniciarTV() {
		List<Integer> canais = Arrays.asList(2,5,7,9,11);	
		televisao = new TV(canais);
		controle = new ControleRemoto(televisao);
		
	}
	
	@Test
	public void teste1Ligar() {		
		assertEquals(true,controle.ligar());
	}
	
	@Test
    public void teste7Desligar() {      
        assertEquals(false,controle.desligar());
    }
	
	@Test
    public void teste5AumentarVolume() {   
		controle.aumentarVolume();
		controle.aumentarVolume();
		controle.aumentarVolume();
		controle.aumentarVolume();
		controle.aumentarVolume();
		assertEquals(5,controle.aumentarVolume());
    }
	
	@Test
    public void teste6DiminuirVolume() {  
		controle.aumentarVolume();
		controle.aumentarVolume();
        assertEquals(1,controle.diminuirVolume());
        assertEquals(0,controle.diminuirVolume());
    }
	
	@Test
    public void teste2MudarCanal() {      
        assertEquals(9,controle.mudarCanal(9));
        assertEquals(11,controle.aumentarCanal());
        assertEquals(9,controle.diminuirCanal());
        assertEquals(7,controle.diminuirCanal());
        assertEquals(5,controle.diminuirCanal());
        assertEquals(11,controle.mudarCanal(11));
        assertEquals(11,controle.aumentarCanal());
    }
	
	@Test
    public void teste3AumentarCanal() {      
        assertEquals(5,controle.aumentarCanal());
    }
	
	@Test
    public void teste4DiminuirCanal() {      
        assertEquals(2,controle.diminuirCanal());
    }

}
