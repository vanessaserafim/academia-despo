package br.gov.serpro.tv;

/*
 * - construtor (TV)
- muda o canal (atrav�s de n�mero, e aumentar e diminuir canal [setinhas])
- liga e desliga
- aumenta diminui volume (setinhas)
 */
public class ControleRemoto {

	private TV televisao;

	public ControleRemoto(TV tv1) {
		televisao = tv1;

	}

	public int mudarCanal(int canal) {
		return televisao.mudarCanal(canal);
	}

	public int  aumentarCanal() {
	   return  televisao.aumentarCanal();
	}

	public int diminuirCanal() {
	   return  televisao.dimunuirCanal();
	}

	public boolean ligar() {
		return televisao.ligar();
	}

	public boolean desligar() {
		return televisao.desligar();
	}

	public int aumentarVolume() {
		return televisao.aumentarVolume();
	}

	public int  diminuirVolume() {
		return televisao.diminuirVolume();

	}

}
