package br.gov.serpro.tv;

import java.util.ArrayList;
import java.util.List;

/*
 * - construtor (array de canais poss�veis)
- canal ativo
- canais poss�veis (array)
- volume (0 a 50)
- ligada ou n�o

 */
public class TV {

	private int canalAtivo = 0;
	private List<Integer> canalPossivel = new ArrayList<>();
	private int volume =0;
	private boolean ligado;

	// cosntrutor
	public TV(List<Integer> canal) {
		canalPossivel = canal;
		canalAtivo = canalPossivel.get(0);
	}

	public int mudarCanal(int canal) {
		for (Integer canalCorrente : canalPossivel) {
			if (canalCorrente.equals(canal)) {
				canalAtivo = canalCorrente;
				break;
			}
		}
		return canalAtivo;
	}

//Iterando a lista e guardando as posicoes
	public int aumentarCanal() {
		int posicao = 0;
		for (Integer canalCorrente : canalPossivel) {
			posicao++;
			// Testa se nao esta no final da lista de canais
			if (canalCorrente.equals(canalAtivo) && posicao == canalPossivel.size()) {
				return canalAtivo;
			} else if(canalCorrente.equals(canalAtivo)){ // Pega a proxima posicao da lista ja que nao � a ultima posicao
				canalAtivo = canalPossivel.get(posicao++);
				return canalAtivo;
			}
		}return canalAtivo;
	}
	
	public int dimunuirCanal() {
        int posicao = 0;
        for (Integer canalCorrente : canalPossivel) {
            posicao++;
            // Testa se nao esta no final da lista de canais
            if (canalCorrente.equals(canalAtivo) && posicao == 1) {
                return canalAtivo;
            } else if(canalCorrente.equals(canalAtivo)){ // Pega a proxima posicao da lista ja que nao � a ultima posicao
                posicao = posicao - 2;
                canalAtivo = canalPossivel.get(posicao);
                return canalAtivo;
            }
        }return canalAtivo;
    }

	public int  aumentarVolume() {
		if (volume < 5) {
			this.volume = volume+1;
		} return volume;
	}

	public int diminuirVolume() {
		if (volume > 0) {
			this.volume = volume-1;
		} return volume;
	}

	public boolean ligar() {
		this.ligado = true;
		return ligado;
	}

	public boolean desligar() {
		this.ligado = false;
		return ligado;
	}
}
