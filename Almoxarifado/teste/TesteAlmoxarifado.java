import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
/*
 * O almoxarifado de uma empresa estoca materiais e deve fazer o controle disso. Para cada material ali depositado, 
 * deve-se saber o seu c�digo num�rico, seu nome e o seu saldo (quantidade em estoque no momento). O saldo pode ser alterado quando 
 * h� retiradas ou abastecimentos, o nome pode ser mudado e deve-se mostrar os dados do material quando necess�rio. 
 * O sistema deve permitir as opera��es de entrada e sa�da do estoque e montar uma lista de produtos em estoque.
 */

public class TesteAlmoxarifado {

	@Test
	public void testQtdProdutosEstocados() {
		//Dado que
		//Eu tenho estoque vazio
		Estoque estoque = new Estoque(); //atalho ctrl+1 para criar a variavel
		
		//quando
		//requisitar a qtd total de materiais
		int qtdTotalDeMateriais = estoque.obterQtdTotalDeMateriais();
		
		//Entao
		//Devo retornar zero
		assertEquals(0, qtdTotalDeMateriais);
	}


	@Test
    public void testListaProdutosEstocados() {
        //Dado que
        //Eu tenho estoque com materiais
        criarEstoque();
       
        //quando
        //requisitar a qtd total de materiais
        List <String> material = new ArrayList<>();
        for (int i = 0; i < material.size(); i++) {
            Estoque estoque;
			material.add(estoque.obterQtdTotalDeMateriais()); //ta errado
        }
        
        //Entao
        //Devo retornar lista dos nomes dos produtos
        //assertEquals(0, qtdTotalDeMateriais);
    }

    private void criarEstoque() {
        
        
    }
	

	
	@Test
	public void testQtdEmEstoquedosMateriais() {
		//Dado que
		//tenha estoque com materiais
		Estoque estoque = new Estoque();
		int codigoNumerico = 1;
		Material material1 = new Material(codigoNumerico);
		estoque.abastecer(material1);
		
		
		//quando
		//eu requisitar a qtd de materiais no estoque
		Integer qtdTotalDeMateriais = estoque.obterQtdTotalDeMateriais();
		
		
		//entao
		//devo listar alguma coisa
		assertEquals(1, (int)estoque.obterQtdTotalDeMateriais());
	}
	
	@Test
	public void testQtdEmEstoqueAposUmaRetirada() {
		
		//dado que
		//eu tenha um material no estoque		
		int codigoNumerico = 1;
		Material material1 = new Material(codigoNumerico);		
		Estoque estoque = new Estoque();
		estoque.abastecer(material1);
		
		//quando
		//eu retira-lo		
		estoque.retirar(material1);
		
		
		//entao
		//meu estoque deve ficar vazio
		assertEquals(0, (int)estoque.obterQtdTotalDeMateriais());
	}
	
	@Test
	public void testQtdEmEstoqueDematerialInexistente() {
		//dado que
		//eu tenha material 1 no estoque		
		int codigoNumerico = 1;
		Material material1 = new Material(codigoNumerico);		
		Estoque estoque = new Estoque();
		estoque.abastecer(material1);
		

		//quando
		//eu tentar retirar	um material diferente do estocado
//		int codNumericodeOutroMaterial = 2;
		Material outroMaterial = new Material(2);
		estoque.retirar(outroMaterial);
		
		
		//entao
		//a qtd deve ser a mesma
		assertEquals(1, (int)estoque.obterQtdTotalDeMateriais());
		
	}

}
