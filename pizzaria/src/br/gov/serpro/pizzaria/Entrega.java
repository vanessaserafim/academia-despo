package br.gov.serpro.pizzaria;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class Entrega {

	int distancia;
	String diaSemana;
	
	//sim, eu copiei da net :P
	public void weekDay() {
		int cal = Calendar.DAY_OF_WEEK;
		this.diaSemana = new DateFormatSymbols().getWeekdays()[cal];
	}
	
	public void calcularDistancia(String endereco) {
		if (endereco == "rua A") {
			this.distancia = 5;
		}else if (endereco == "rua B") {
			this.distancia = 10;
		}else {
			this.distancia = 20;
		}
	}
	
	public int calcularFrete(String endereco){
		
	    //Quarta frete gratis
		weekDay();
	    if (this.diaSemana == "quarta"){
	        return 0;
	    }
	    calcularDistancia(endereco);
	    
	    //Valor do frete se nao for quarta
	    if (this.distancia <= 5){
	        return 5;	        
	    }else if (this.distancia > 5 && this.distancia <= 10){
	        return 10;
	    }else{
	        return 15;
	    }
	    
	    
	}
	
	
	
}
