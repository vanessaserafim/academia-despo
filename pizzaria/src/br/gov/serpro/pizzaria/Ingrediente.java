package br.gov.serpro.pizzaria;

public class Ingrediente {
	
	//double valor;
	String sabor;
	
	public String getSabor() {
        return sabor;
    }

    public void setSabor(String sabor) {
        this.sabor = sabor;
    }

    public int calcularValorBase(String sabor) {
		
		if (sabor == "calabresa") {
			return 30;
		} else if (sabor == "queijo") {
			return 20;
		}else {
			return 50;
		}
	}

}
