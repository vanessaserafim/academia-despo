package br.gov.serpro.pizzaria;

/**
 * @startuml
class Pizza [[java:br.gov.serpro.pizzaria.Pizza]] {
    ~int quantidadeSabor
    ~Ingrediente ingrediente1
    ~Ingrediente ingrediente2
    +Pizza(int quantidadeSabor, Ingrediente sabor1, Ingrediente sabor2)
    +Pizza(int quantidadeSabor, Ingrediente sabor1)
    +int calcularValorPizza(Pizza pizza)
}


class Ingrediente [[java:br.gov.serpro.pizzaria.Ingrediente]] {
    ~String sabor
    +String getSabor()
    +void setSabor(String sabor)
    +int calcularValorBase(String sabor)
}

Pizza o-- Ingrediente
@enduml
 * @author 77989104504
 *
 */

public class Pizza {
	int quantidadeSabor;
	Ingrediente ingrediente1;
	Ingrediente ingrediente2;

	
	public Pizza(int quantidadeSabor, Ingrediente sabor1, Ingrediente sabor2) {
		this.quantidadeSabor = quantidadeSabor;
		this.ingrediente1 = sabor1;
		this.ingrediente2 = sabor2;
	}
	
    public Pizza(int quantidadeSabor, Ingrediente sabor1) {
        this.quantidadeSabor = quantidadeSabor;
        this.ingrediente1 = sabor1;
    }

    public int calcularValorPizza(Pizza pizza) {		
		if (this.quantidadeSabor == 1) {
			int valorsabor1 = ingrediente1.calcularValorBase(pizza.ingrediente1.sabor);	//estranhissimo
			return valorsabor1;
			
	    }else {
	    	int valorsabor1 = ingrediente1.calcularValorBase(pizza.ingrediente1.sabor);
			int valorsabor2 = ingrediente2.calcularValorBase(pizza.ingrediente2.sabor);	
			
			if ( valorsabor1 < valorsabor2) {
		        return valorsabor2;	        
		    }else{
		        return valorsabor1;
		    }
	    }
	    	
	    
	}

}
