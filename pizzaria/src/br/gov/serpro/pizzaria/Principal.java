package br.gov.serpro.pizzaria;

/*
 *  classe pizza, que seria respons�vel ali por estar calculando o pre�o baseado nos ingredientes,
 *  poder�amos ter uma classe entrega, que estaria ali respons�vel por calcular o valor do frete,
 *  e a gente teria uma classe a� que seria o carrinho de compras, que � uma met�fora muito comum
 *  principalmente quando a gente trabalha com e-commerce, que seria onde por exemplo normalmente, 
 *  muitas vezes a pessoa vai comprar, ela n�o compra uma pizza s�, ela compra �s vezes v�rias pizzas, 
 *  compra ali refrigerante, pode comprar uma sobremesa. Ent�o esse carrinho seria onde teriam todos 
 *  os produtos que a pessoa comprou n�? E tamb�m quando eu perguntasse o total desse carrinho, ele 
 *  me daria o somat�rio do pre�o dos produtos mais o pre�o da entrega, que seria a� o pre�o total.
 * 
 */
public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	    Ingrediente sabor1 = new Ingrediente();
	    sabor1.setSabor("queijo");
	    Ingrediente sabor2 = new Ingrediente();
        sabor2.setSabor("calabresa");
        Ingrediente sabor3 = new Ingrediente();
        sabor3.setSabor("camarao");
        
	    //instanciar pizza
	    Pizza pizza1 = new Pizza(1 ,sabor1);
	    Pizza pizza2 = new Pizza(2 ,sabor1,sabor2);
	    Pizza pizza3 = new Pizza(2 ,sabor2,sabor3);
	    
	    //Instanciar carrinhos
	    Carrinho carrinho1 = new Carrinho();
	    carrinho1.setEndereco("rua A");
	    carrinho1.setPizza(pizza1);
	  
	    System.out.println ("O valor da pizza1 � " + carrinho1.calcularValorTotal());
	    
	    Carrinho carrinho2 = new Carrinho();
	    carrinho2.setEndereco("rua B");
	    carrinho2.setPizza(pizza2);
	  
	    System.out.println ("O valor da pizza2 � " + carrinho2.calcularValorTotal());
	    
	    Carrinho carrinho3 = new Carrinho();
	    carrinho3.setEndereco("rua C");
	    carrinho3.setPizza(pizza3);
	  
	    System.out.println ("O valor da pizza3 � " + carrinho3.calcularValorTotal());
	    
	    
	}

}
