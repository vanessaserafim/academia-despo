package br.gov.serpro.pizzaria;

public class Carrinho {
	
	Pizza pizza;
	String endereco;

	
	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}


	//calcular valor total
	public int calcularValorTotal(){
		Entrega pedido = new Entrega();
		
	    return pizza.calcularValorPizza(this.pizza) + pedido.calcularFrete(this.endereco);
	    
	}
	
	//
	

}
