package br.gov.serpro.pizzaria;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestePizzaria {
    
    private Pizza criarPizzaUmSabor(){
        Ingrediente sabor1 = new Ingrediente();
        sabor1.setSabor("queijo");            
        //instanciar pizza
        Pizza pizza1 = new Pizza(1 ,sabor1);
        return pizza1;
    }
    
    private Pizza criarPizzaDoisSabores(){
        Ingrediente sabor1 = new Ingrediente();
        sabor1.setSabor("queijo");
        Ingrediente sabor2 = new Ingrediente();
        sabor2.setSabor("calabresa");
        //instanciar pizza
        Pizza pizza2 = new Pizza(2 ,sabor1,sabor2);
        return pizza2;
    }
    
    
    private Pizza criarPizzaOutrosSaboresDiferentes(){
        Ingrediente sabor2 = new Ingrediente();
        sabor2.setSabor("calabresa");
        Ingrediente sabor3 = new Ingrediente();
        sabor3.setSabor("camarao");        
        Pizza pizza3 = new Pizza(2 ,sabor2,sabor3);
        return pizza3;
    }
    
    private Carrinho criarCarrinho(Pizza p){
        Carrinho carrinho1 = new Carrinho();
        carrinho1.setEndereco("rua A");
        carrinho1.setPizza(p);
        return carrinho1;
    }
    

	@Test
	public void testCalcularValorTotal() {
		Pizza pizza1 = criarPizzaUmSabor();
		Carrinho carrinho1 = criarCarrinho(pizza1);
		assertEquals(25, carrinho1.calcularValorTotal());
		
		Pizza pizza2 = criarPizzaDoisSabores();
        Carrinho carrinho2 = criarCarrinho(pizza2);
        assertEquals(35, carrinho2.calcularValorTotal());
        
        Pizza pizza3 = criarPizzaOutrosSaboresDiferentes();
        Carrinho carrinho3 = criarCarrinho(pizza3);
        assertEquals(55, carrinho3.calcularValorTotal());
	
		
	}
	
	@Test
	public void testcalcularValorBase(){
	    Pizza pizza1 = criarPizzaDoisSabores();
	    assertEquals(20, pizza1.ingrediente1.calcularValorBase(pizza1.ingrediente1.sabor)); //estranhissimo
	}
	
   @Test
    public void testCalcularValorPizza(){
        
       Pizza pizza1 = criarPizzaDoisSabores();
       assertEquals(30, pizza1.calcularValorPizza(pizza1));//estranhissimo
       
       Pizza pizza2 = criarPizzaOutrosSaboresDiferentes();
       assertEquals(50, pizza2.calcularValorPizza(pizza2));//estranhissimo
        
    }
	

}
