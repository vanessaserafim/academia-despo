package br.gov.serpro.carros;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TesteCarro {

	@Test
	void test() {
		Carro c = new Carro();
		c.potencia=10;
		c.velocidade = 50;
		
		c.acelerar();
		assertEquals(60, c.getVelocidade());
		
		Carro c2 = new Carro();
		c2.velocidade = 10;
		
		c2.frear();
		assertEquals(5, c2.getVelocidade());
		
		
	}

}
