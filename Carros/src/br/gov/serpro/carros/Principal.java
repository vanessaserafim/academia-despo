package br.gov.serpro.carros;

public class Principal {

	public static void main(String[] args) {
		Carro uno = new Carro();
		//uno.velocidade = 15;
		uno.setVelocidade(15);
		//uno.potencia = 20;
		uno.setVelocidade(20);
		//uno.nome = "Uno Mille";
		uno.setNome("Uno MIlle");
		
		uno.acelerar();
		uno.acelerar();
		uno.imprimir();
		
		Carro bmw = new Carro();
		bmw.nome ="i320";
		bmw.potencia=50;
		bmw.velocidade = 10;
		
		bmw.acelerar();
		bmw.acelerar();
		bmw.acelerar();
		bmw.acelerar();
		bmw.acelerar();
		bmw.acelerar();
		bmw.frear();
		
		bmw.imprimir();
		
		

	}

}
