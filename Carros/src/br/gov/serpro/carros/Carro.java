package br.gov.serpro.carros;

public class Carro {
	
	private int potencia;
	private int velocidade;
	private int velocidadeMaxima;
	private String nome;
	
	
	
	
	public Carro(int potencia, int velocidadeMaxima, String nome) {
		super();
		this.potencia = potencia;
		this.velocidadeMaxima = velocidadeMaxima;
		this.nome = nome;
	}

	void acelerar() {
		velocidade = velocidade + potencia;
	}
	
	void frear() {
		velocidade = velocidade / 2;
	}
	
	
	void  imprimir() {
		System.out.println("O carro " + nome  + " est� na velocidade " + getVelocidade() );
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public int getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(int velocidade) {
		this.velocidade = velocidade;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	

}
