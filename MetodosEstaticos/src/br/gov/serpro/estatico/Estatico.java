package br.gov.serpro.estatico;

/*Criar uma classe com m�todos est�ticos para calcular:
- Dado 2 n�meros, retorna o maior deles;
- Dada uma temperatura em graus celsius, retorna o valor em fahrenheit;
- Dado 2 n�meros, retornar um n�mero aleat�rio entre os 2 (ex: passados 1 e 4, pode retornar aleatoriamente 1, 2, 3 ou 4);
- Dado um n�mero de CPF, retorna TRUE caso seja um n�mero v�lido (validando inclusive seu digito verificador) ou FALSE caso contr�rio;
- Dado um n�mero de CPF (n�o formatado), retorna uma String com CPF formatado (ex: dado 30723401888 retorna 307.234.018-88);*/

public class Estatico {
    
    //- Dado 2 n�meros, retorna o maior deles;
    public static long maiorNumero(int num1, int num2){
        
        if (num1 > num2){
            return num1;
        }else if (num1 < num2) {
           return num2;
        }else {
            return -1; //default caso sejam iguais
        }
        
    }
    
    //- Dada uma temperatura em graus celsius, retorna o valor em fahrenheit;
    public static double toFahrenheit(double celsius){        
        return ((celsius * 9 + 160)/5);           
    }

    //- Dado 2 n�meros, retornar um n�mero aleat�rio entre os 2 
    public static int aleatorio(int num1, int num2){
        return num1 + (int)(Math.random() * (num2 - num1));
    }
    
    //- Dado um n�mero de CPF, retorna TRUE caso seja um n�mero v�lido 
     public static boolean validarCpf(String cpf){  
        if ( cpf == null ){
            return false;
        }
        else{
                String cpfGerado = "";     
                //removerCaracteres();
                cpf = cpf.replace("-","");
                cpf = cpf.replace(".","");   
            if ( verificarSeTamanhoInvalido(cpf) )
                 return false;       
            if ( verificarSeDigIguais(cpf) )
                 return false;            
            cpfGerado = cpf.substring(0, 9);
            cpfGerado = cpfGerado.concat(calculoComCpf(cpfGerado));
            cpfGerado = cpfGerado.concat(calculoComCpf(cpfGerado));
            
            if ( !cpfGerado.equals(cpf))
                 return false;
        } 
        return true;
    }
     

        private static boolean verificarSeTamanhoInvalido(String cpf){  
            if ( cpf.length() != 11 )
              return true;   
                return false;
        } 
        
        private static boolean verificarSeDigIguais(String cpf){   
            char primDig = '0';
            char [] charCpf = cpf.toCharArray();  
               for( char c: charCpf  )
                   if ( c != primDig )
                   return false;        
                   return true;
        } 
        
        private static String calculoComCpf(String cpf){   
         int digGerado = 0;
         int mult = cpf.length()+1;
         char [] charCpf = cpf.toCharArray();
         for ( int i = 0; i < cpf.length(); i++ )
              digGerado += (charCpf[i]-48)* mult--;
         if ( digGerado % 11 < 2)
              digGerado = 0;
         else
             digGerado = 11 - digGerado % 11;
         return  String.valueOf(digGerado); 
        }
    
    
    
    //- Dado um n�mero de CPF (n�o formatado), retorna uma String com CPF formatado
    public static String formataCpf(String cpf){
        String cpfFormatado = cpf;
        if (cpfFormatado.length() < 11) {
            return "Numero invalido";
        }else{
            String bloco1 = cpf.substring(0, 3);
            String bloco2 = cpf.substring(3, 6);
            String bloco3 = cpf.substring(6, 9);
            String bloco4 = cpf.substring(9, 11);
            cpfFormatado = String.format("%s.%s.%s-%s", bloco1, bloco2, bloco3, bloco4);
            return cpfFormatado;
        }
    }
    
}
