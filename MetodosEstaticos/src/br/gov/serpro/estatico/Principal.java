package br.gov.serpro.estatico;

public class Principal {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
        int num1 = 15;
        int num2 = 30;
        String cpf = "12345678900";
        
        System.out.println("Maior numero: " + Estatico.maiorNumero(num1, num2));
        
        System.out.println("Numero aleatorio: " + Estatico.aleatorio(num1, num2));
        
        System.out.println("Em Fahrenheit: " + Estatico.toFahrenheit(num1));
        
        System.out.println("CPF formatado:" + Estatico.formataCpf(cpf));
        
        System.out.println("Cpf valido: " + Estatico.validarCpf(cpf));

    }

}
