package br.gov.serpro.imc;

import static org.junit.Assert.*;

import org.junit.Test;

public class TesteIMC {

	private Paciente criarPaciente(int peso, double altura){
		Paciente fulano = new Paciente(peso, altura);
		return fulano;
	}
	
	@Test
	public void testDiagnosticoMortoDeFome() {
		Paciente fulano = new Paciente(50, 1.9);  
		assertEquals("Baixo peso muito grave = IMC abaixo de 16 kg/m�", fulano.diagnostico());
	}
	
	@Test
	public void testDiagnosticoDesnutrido() {
		Paciente fulano = criarPaciente(60, 1.9); //Outro jeito caso a criacao da instancia seja mais elaborada.
		assertEquals("Baixo peso grave = IMC entre 16 e 16,99 kg/m�", fulano.diagnostico());
	}
	
	@Test
	public void testDiagnosticoMagrinho() {
		Paciente fulano = new Paciente(65, 1.9); 
		assertEquals("Baixo peso = IMC entre 17 e 18,49 kg/m�", fulano.diagnostico());
	}
	
	@Test
	public void testDiagnosticoNormal() {
		Paciente fulano = new Paciente(70, 1.9); 
		assertEquals("Peso normal = IMC entre 18,50 e 24,99 kg/m�", fulano.diagnostico());
	}

	@Test
	public void testDiagnosticoSobrepeso() {
		Paciente fulano = new Paciente(95, 1.9); 
		assertEquals("Sobrepeso = IMC entre 25 e 29,99 kg/m�", fulano.diagnostico());
	}
	
	@Test
	public void testDiagnosticoObesoI() {
		Paciente fulano = new Paciente(115, 1.9); 
		assertEquals("Obesidade grau I = IMC entre 30 e 34,99 kg/m�", fulano.diagnostico());
	}
}
