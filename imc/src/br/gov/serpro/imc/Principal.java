package br.gov.serpro.imc;

/*Implemente no Eclipse uma classe chamada Paciente que possui um construtor que recebe o seu peso em quilos e sua altura em metros, ambos utilizando o 
 * tipo double. Crie um m�todo chamado calcularIMC() que calcula o �ndice de Massa Corporal de acordo com a f�rmula 
 * IMC = peso (quilos) / (altura * altura (metros)). Crie tamb�m um m�todo chamado diagnostico() que utiliza o m�todo calcularIMC() e 
 * retorna uma String de acordo com as seguintes faixas de valor:

Baixo peso muito grave = IMC abaixo de 16 kg/m�
Baixo peso grave = IMC entre 16 e 16,99 kg/m�
Baixo peso = IMC entre 17 e 18,49 kg/m�
Peso normal = IMC entre 18,50 e 24,99 kg/m�
Sobrepeso = IMC entre 25 e 29,99 kg/m�
Obesidade grau I = IMC entre 30 e 34,99 kg/m�
Obesidade grau II = IMC entre 35 e 39,99 kg/m�
Obesidade grau III (obesidade m�rbida) = IMC igual ou maior que 40 kg/m�
Implemente no Eclipse uma classe chamada Principal em que se criam 3 inst�ncias da classe Paciente com valores diferentes e se imprime no console o resultado dos dois m�todos criados.
*/


public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Paciente fulano = new Paciente(80, 1.80);
		Paciente cicrano = new Paciente(50,1.65);
		Paciente beltrano = new Paciente(90, 1.60);
		
		System.out.println("O IMC de fulnao � " + fulano.diagnostico());
		System.out.println("O imc de cicrano � " + cicrano.diagnostico());
		System.out.println("O imc de beltrano � " + beltrano.diagnostico());
		
	}

}
