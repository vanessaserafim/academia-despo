package br.gov.serpro.interfaces;

public interface Conexao {
	
	public void rollback();
	
	public void commit();
	
	public void conectar();
	

}
