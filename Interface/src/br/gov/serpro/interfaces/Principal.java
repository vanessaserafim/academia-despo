package br.gov.serpro.interfaces;

public class Principal {
	
	public static void conexaoGenerica(Conexao banco) {		
		banco.conectar();
		banco.commit();
		banco.rollback();
	}

	public static void main(String[] args) {
		conexaoGenerica( new Oracle());
		conexaoGenerica( new SqlServer());
		
		/*conexao banco = new Oracle;
		 * banco.commit();
		banco.conectar();
		banco.rollback();*/

	}

}
