package br.gov.serpro.interfaces;

public class Oracle implements Conexao{
	
	public void rollback() {
		System.out.println("rollback oracle");
	}
	
	public void commit() {
		System.out.println("commit oracle");
	}
	
	public void conectar() {
		System.out.println("conectar oracle");
	}
	
	

}
