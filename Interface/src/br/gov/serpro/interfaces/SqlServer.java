package br.gov.serpro.interfaces;

public class SqlServer implements Conexao {
	
	public void rollback() {
		System.out.println("rollback sqlserver");
	}
	
	public void commit() {
		System.out.println("commit sqlserver");
	}
	
	public void conectar() {
		System.out.println("conectar sqlserver");
	}

}
