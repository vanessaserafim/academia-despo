package br.gov.serpro.cofrinho;

import java.util.ArrayList;
import java.util.List;

/*Exerc�cio 1
Implementar:
Deseja-se implementar um cofrinho de moedas com a capacidade de receber moedas e calcular o total depositado no cofrinho. 

Contar o n�mero de moedas armazenadas;
Contar o n�mero de moedas de um determinado valor;
Informar qual a moeda de maior valor.*/

public class Cofrinho {
    
    List<Double> moedas = new ArrayList<>();
    
    //receber moedas
    public void receberMoeda(double valor) {      
        moedas.add(valor);
    }
    
    //Total cofrinho
    public Double totalCofrinho(){
        double totalCofrinho = 0;
        
        for (int i = 0; i < moedas.size(); i++) {
            totalCofrinho = totalCofrinho + moedas.get(i).doubleValue();
        } 
        
        return totalCofrinho;
        
    }
    
    //Contar o n�mero de moedas armazenadas;.
    public int contarMoedasCofrinho(){
        int totalMoedas= moedas.size(); //ja retorna o o tamanho do  array nao precisa de for
      /*  for (int i = 0; i < moedas.size(); i++) {
            totalMoedas++;
        }*/ 
        
        return totalMoedas;
    }
    
    //Contar o n�mero de moedas de um determinado valor;
    public int contarMoedasPorValor( double valor) {

        int totalMoeda = 0;
        for (int i = 0; i < moedas.size(); i++) {
            if (moedas.get(i).equals(valor)){
               totalMoeda++;        
            }
        }
        return totalMoeda;
    }
    
    
}
