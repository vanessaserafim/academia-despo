package br.gov.serpro.cofrinho;

public class Principal {

    public static void main(String[] args) {
        Cofrinho cofrinho = new Cofrinho();
        
        cofrinho.receberMoeda(.25);
        cofrinho.receberMoeda(.10);
        cofrinho.receberMoeda(.05);
        cofrinho.receberMoeda(1);
        cofrinho.receberMoeda(.10);
        cofrinho.receberMoeda(.5);
        
        System.out.println("Total do cofrinho:" + cofrinho.totalCofrinho());
        
        System.out.println("Qtd moedas: \n");
        System.out.println("0.01: " + cofrinho.contarMoedasPorValor(.01));        
        System.out.println(" 0.05: " + cofrinho.contarMoedasPorValor(.05));
        System.out.println(" 0.10: " + cofrinho.contarMoedasPorValor(.10));
        System.out.println(" 0.25: " + cofrinho.contarMoedasPorValor(.25));
        System.out.println(" 0.50: " + cofrinho.contarMoedasPorValor(.50));
        System.out.println(" 1.00: " + cofrinho.contarMoedasPorValor(1));
        
        System.out.println("Quantidade total de moedas do cofrinho: " + cofrinho.contarMoedasCofrinho());

    }

}
