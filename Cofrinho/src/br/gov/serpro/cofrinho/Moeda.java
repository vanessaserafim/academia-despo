package br.gov.serpro.cofrinho;

public class Moeda {
	
private ValorMoeda valorMoeda;
	
	/**
	 * N�o foi pedido no exerc�cio mas faz parte da discuss�o de modelagem OO 
	 */
	private String imagem;
	private boolean isComemorativa;
	
	
	Moeda(ValorMoeda valorMoeda) {
		this.valorMoeda = valorMoeda;
	}

	public ValorMoeda getValorMoeda() {
		return valorMoeda;
	}
	
	public Double getValorEmReais() {
		return valorMoeda.getValor();
	}


}
