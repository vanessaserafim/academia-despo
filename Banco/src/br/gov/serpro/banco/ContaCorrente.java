package br.gov.serpro.banco;

public class ContaCorrente {
	int saldo;
	
	public int sacar(int valor) {
		if( valor < saldo) {
			saldo = saldo - valor;
			return valor;
		}else{
			return 0;
		}
	}
	
	
	public int getSaldo() {
		return saldo;
	}


	public void depositar(int valor) {
		saldo =saldo + valor;
	}

}
