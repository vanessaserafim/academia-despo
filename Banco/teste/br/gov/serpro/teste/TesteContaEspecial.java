package br.gov.serpro.teste;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import br.gov.serpro.banco.ContaEspecial;

public class TesteContaEspecial {

	ContaEspecial cc;
	
	@Before
	public void inicializarConta() {
		cc = new ContaEspecial(100);
		cc.depositar(250);
	}
	
	
	@Test
	public void saqueMaiorQueLimite() {		
		cc.sacar(400);
		assertEquals(250, cc.getSaldo());
	}
	
	@Test
	public void saqueDentroLimite() {		
		cc.sacar(300);
		assertEquals(-50, cc.getSaldo());
	}
}
