package br.gov.serpro.teste;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.gov.serpro.banco.ContaCorrente;

public class TesteContaCorrente {
	ContaCorrente cc;
	
	@Before
	public void inicializarConta() {
		cc = new ContaCorrente();
	}
	
	@Test
	public void depositar() {		
		cc.depositar(200);
		assertEquals(200, cc.getSaldo());
	}

	@Test
	public void sacar() {		
		cc.depositar(200);
		cc.sacar(100);
		assertEquals(100, cc.getSaldo());
	}

	@Test
	public void saqueMaiorQueSaldo() {
		cc.depositar(100);
		cc.sacar(150);
		assertEquals(100, cc.getSaldo());
	}

}
